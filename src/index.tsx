/*
 * @Date: 2021-08-08 12:31:26
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-19 22:33:07
 * @Description:
 */
import ReactDOM from 'react-dom';
import './assets/css/index.css';
import reportWebVitals from './reportWebVitals';
import App from './App'

ReactDOM.render(
    <App></App>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
