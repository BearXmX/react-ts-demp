/*
 * @Date: 2021-08-08 12:46:52
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-05 12:14:31
 * @Description:
 */
import { Form, Input, Button, Checkbox, DatePicker } from 'antd';
import { useEffect, useState, useContext } from 'react';
import { SonContext } from '../../App';
import moment from 'moment';
import './index.less';

const { RangePicker } = DatePicker;
interface SonProps {
    sendApp: any;
    getApp: string;
    date: any;
}

const Son: React.FC<SonProps> = (props: SonProps) => {
    const count = useContext(SonContext);
    console.log(count, 'useContext');
    const [initObj, setInitObj] = useState({
        pageIndex: 1,
        pageSize: 10,
        where: {
            keyword: '关键字',
        },
    });
    const onFinish = (values: any) => {
        console.log('Success:', values);
        props.sendApp(values);
    };

    const onFinishFailed = (errorInfo: any) => {};

    const onFieldsChange = (changedFields: any, allFields: any) => {
        console.log('onFieldsChange:', changedFields, allFields);
    };
    const changeInitObj = () => {
        setInitObj({ ...initObj, where: { keyword: '关键词' }, pageSize: 100 });
    };

    const onChange = (date: any, dateString: any) => {
        console.log(date, dateString);
    };

    useEffect(() => {
        console.log(props);
    }, []);

    useEffect(() => {
        console.log(initObj);
    }, [initObj]);
    return (
        <>
            <div className="myForm">
                <RangePicker
                    showTime
                    placeholder={['开始日期', '结束日期']}
                    format="YYYY-MM-DD HH:mm:ss"
                    value={
                        props.date
                            ? [
                                  moment(props.date[0], 'YYYY-MM-DD HH:mm:ss'),
                                  moment(props.date[1], 'YYYY-MM-DD HH:mm:ss'),
                              ]
                            : null
                    }
                    //defaultValue={[moment('2021-07-10 20:23:35', "YYYY-MM-DD HH:mm:ss"), moment('2021-08-11 20:23:35', "YYYY-MM-DD HH:mm:ss")]}
                    onChange={onChange}
                />
                <Button onClick={changeInitObj}>测试监听</Button>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    onFieldsChange={onFieldsChange}
                >
                    <Form.Item
                        label="Username"
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        name="remember"
                        valuePropName="checked"
                        wrapperCol={{ offset: 8, span: 16 }}
                    >
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </>
    );
};

export default Son;
