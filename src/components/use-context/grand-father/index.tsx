/*
 * @Date: 2021-09-14 21:37:48
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-14 22:17:18
 * @Description: 爷爷组件
 */
import { useContext } from 'react';
import { myContext } from '../../../page/hooks-test/use-context';
import Father from '../father';
const GrandFather: React.FC = () => {
    const { time, randon } = useContext(myContext);
    return (
        <div
            style={{
                marginTop: 10,
                padding: 20,
                border: '1px solid #f7acbc',
                height: 300,
            }}
        >
            GrandFather
            <div>
                <code style={{ color: '#d71345', marginRight: 20 }}>
                    {time}
                </code>
                <code style={{ color: '#973c3f' }}>{randon}</code>
            </div>
            <Father></Father>
        </div>
    );
};

export default GrandFather;
