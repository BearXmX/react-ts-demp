/*
 * @Date: 2021-09-14 21:41:46
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-14 22:17:37
 * @Description: 孙组件
 */
import { useContext } from 'react';
import { myContext } from '../../../page/hooks-test/use-context';

const Son: React.FC = () => {
    const { time, randon } = useContext(myContext);

    return (
        <div
            style={{
                marginTop: 10,
                padding: 20,
                border: '1px solid #2b4490',
                height: 100,
            }}
        >
            Son
            <div>
                <code style={{ color: '#224b8f',marginRight: 20 }}>{time}</code>
                <code style={{ color: '#426ab3' }}>{randon}</code>
            </div>
        </div>
    );
};

export default Son;
