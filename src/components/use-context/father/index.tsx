/*
 * @Date: 2021-09-14 21:40:15
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-14 22:17:24
 * @Description: 父组件
 */
import { useContext } from 'react';
import { myContext } from '../../../page/hooks-test/use-context';
import Son from '../son';
const Father: React.FC = () => {
    const { time, randon } = useContext(myContext);
    return (
        <div
            style={{
                marginTop: 10,
                padding: 20,
                border: '1px solid #77ac98',
                height: 200,
            }}
        >
            Father
            <div>
                <code style={{ color: '#45b97c', marginRight: 20 }}>
                    {time}
                </code>
                <code style={{ color: '#40835e' }}>{randon}</code>
            </div>
            <Son></Son>
        </div>
    );
};

export default Father;
