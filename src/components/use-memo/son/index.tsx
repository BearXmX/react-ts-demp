/*
 * @Date: 2021-09-19 21:29:40
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-19 22:18:37
 * @Description: useMemo son
 */
import { memo, useState } from 'react';
import { Button } from 'antd';
import moment from 'moment';
const Son: React.FC = () => {
    const [count, setCount] = useState(0);
    console.log('update');
    return (
        <div
            style={{ border: '1px solid #8552a1', padding: 20, marginTop: 20 }}
        >
            useMemo son
            <Button
                type="dashed"
                danger
                style={{ marginLeft: 10 }}
                onClick={() => setCount(count + Math.floor(Math.random() * 60))}
            >
                son change
            </Button>
            <p style={{ color: '#f173ac' }}>
                {moment(+new Date()).format('YYYY-MM-DD HH:mm:ss')}
            </p>
            <p>{count}</p>
        </div>
    );
};

export default memo(Son);
