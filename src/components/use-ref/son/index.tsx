/*
 * @Date: 2021-09-19 13:43:25
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-19 15:17:33
 * @Description: useRef子组件
 */
import React, { forwardRef } from 'react';
import { useImperativeHandle, useState } from 'react';
import { Button } from 'antd';

const Son = (props: any, ref: any) => {
    const [count, setCount] = useState(0);
    useImperativeHandle(ref, () => ({
        getCount: () => {
            return count;
        },
    }));

    return (
        <div
            style={{
                border: '1px solid #73b9a2',
                padding: '20px',
                marginTop: 20,
            }}
        >
            <Button type="primary" onClick={() => setCount(count + 1)}>
                useRef-son
            </Button>
            <p>{count}</p>
        </div>
    );
};

export default forwardRef(Son);
