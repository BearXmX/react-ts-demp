/*
 * @Date: 2021-09-25 14:17:28
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-25 21:32:19
 * @Description: hoc子组件
 */
import withParams from '../with-params';
type getCurrentTime = () => string | number;
interface myProps {
    getCurrentTime: getCurrentTime;
}

const Son: React.FC<myProps> = (props) => {
    const { getCurrentTime } = props;
    return (
        <div
            style={{ border: '1px solid #008792', padding: 20, marginTop: 20 }}
        >
            <p style={{ color: '#006c54' }}>Hoc Son</p>
            <p style={{ color: '#006c54' }}>时间: {getCurrentTime()}</p>
        </div>
    );
};

export default withParams(Son);
