/*
 * @Date: 2021-09-25 14:13:42
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-25 15:57:35
 * @Description: withParams高阶组件
 */
import moment from 'moment';
const withParams = (EnhanceCompoent: any) => {
    return () => {
        const initParams: object = window.performance.timing || {};
        /**
         * @description: 获取当前时间
         * @param {number} time 时间戳
         * @return {*}
         */
        const getCurrentTime = (): string => {
            return moment(+new Date()).format('YYYY-MM-DD HH:mm:ss');
        };
        return (
            <EnhanceCompoent
                getCurrentTime={getCurrentTime}
                initParams={initParams}
            ></EnhanceCompoent>
        );
    };
};

export default withParams;
