/*
 * @Date: 2021-09-12 18:02:51
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-12 22:34:28
 * @Description: redux状态管理
 */
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from './reducer';

const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware())
);

// 如果有多个reducer 需要连接后使用，这样，在使用 state中的数据，需要 .count
/* const allReducers =combineReducers({
    count:reduce
}) */

export default store;
