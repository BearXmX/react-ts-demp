/*
 * @Date: 2021-09-12 18:09:00
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-12 19:50:01
 * @Description: reducer
 */

import initState from '../state';

const reducer = (state = initState, action: any) => {
    switch (action.type) {
        case 'add':
            return { ...state, count: action.payload };
        case 'reduce':
            return { ...state, count: action.payload };
        default:
            return initState;
    }
};

export default reducer;
