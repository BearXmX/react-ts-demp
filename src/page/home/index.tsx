/*
 * @Date: 2021-09-05 12:38:37
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-25 14:03:04
 * @Description: home页面
 */
import { useState } from 'react';
import { Layout, Menu, Breadcrumb, Card } from 'antd';
import {
    ThunderboltOutlined,
    FileOutlined,
    TeamOutlined,
    ApiOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    PieChartOutlined,
    RiseOutlined
} from '@ant-design/icons';
import { withRouter } from 'react-router-dom';

import './index.less';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const Home: React.FC = (props: any) => {
    const [collapsed, setCollapsed] = useState(false); // 是否折叠sider
    const menuHighLight = window.location.hash.split('#/home')[1];

    /**
     * @description: 折叠sider的回调
     * @param {boolean} collapsed
     */
    const onCollapse = (collapsed: boolean) => {
        setCollapsed(collapsed);
    };

    /**
     * @description: 点击menu菜单的回调
     * @param {any} e
     */
    const menuClick = (e: any) => {
        props.history.push('/home' + e.key);
    };
    /*     const to = () => {
        props.history.push('/login');
    }; */
    return (
        <>
            <Layout style={{ minHeight: '100vh' }}>
                <Sider
                    collapsible
                    collapsed={collapsed}
                    onCollapse={onCollapse}
                    theme="light"
                    trigger={
                        collapsed ? (
                            <MenuUnfoldOutlined />
                        ) : (
                            <MenuFoldOutlined />
                        )
                    }
                >
                    <div className="logo">antd</div>
                    <Menu
                        theme="light"
                        mode="inline"
                        onClick={menuClick}
                        defaultSelectedKeys={[menuHighLight]}
                        defaultOpenKeys={['/hooks', 'sub2']}
                    >
                        <Menu.Item
                            title="cockpit"
                            key="/cockpit"
                            icon={<PieChartOutlined />}
                        >
                            cockpit
                        </Menu.Item>
                        <Menu.Item
                            title="redux"
                            key="/redux"
                            icon={<ThunderboltOutlined />}
                        >
                            redux
                        </Menu.Item>
                        <SubMenu
                            key="/hooks"
                            icon={<ApiOutlined />}
                            title="hooks"
                        >
                            <Menu.Item key="/useContext" title="useContext">
                                useContext
                            </Menu.Item>
                            <Menu.Item key="/useRef" title="useRef">
                                useRef
                            </Menu.Item>
                            <Menu.Item key="/useMemo" title="useMemo">
                                useMemo
                            </Menu.Item>
                        </SubMenu>
                        <Menu.Item
                            title="hoc"
                            key="/hoc"
                            icon={<RiseOutlined />}
                        >
                            hoc
                        </Menu.Item>
                        <SubMenu
                            key="sub2"
                            icon={<TeamOutlined />}
                            title="Team"
                        >
                            <Menu.Item key="6">Team 1</Menu.Item>
                            <Menu.Item key="8">Team 2</Menu.Item>
                        </SubMenu>
                        <Menu.Item key="9" icon={<FileOutlined />}>
                            Files
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header
                        className="site-layout-background"
                        style={{ padding: 0 }}
                    />
                    <Content style={{ margin: '0 16px' }}>
                        <Breadcrumb style={{ margin: '16px 0' }}>
                            <Breadcrumb.Item>
                                {menuHighLight.replace('/', '')}
                            </Breadcrumb.Item>
                        </Breadcrumb>
                        <Card className="visiable-card">{props.children}</Card>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>
                        Ant Design ©2018 Created by Ant UED
                    </Footer>
                </Layout>
            </Layout>
        </>
    );
};

export default withRouter(Home);
