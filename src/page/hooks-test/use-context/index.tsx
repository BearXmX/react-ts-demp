/*
 * @Date: 2021-09-12 22:39:13
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-14 22:18:07
 * @Description: hooks-useContext
 */
import { createContext, useState } from 'react';
import { Button } from 'antd';
import moment from 'moment';
import GrandFather from '../../../components/use-context/grand-father';
interface contextProps {
    time: string;
    randon: number;
}

export const myContext = createContext<contextProps>({
    time: '',
    randon: 0,
});

const UseContext: React.FC = () => {
    const [time, setTime] = useState('ready send time !');
    const [randon, setRandon] = useState(0);

    /**
     * @description: 传递上下文
     */
    const setContext = () => {
        setTime(moment(+new Date()).format('YYYY-MM-DD HH:mm:ss'));
        setRandon(Math.floor(Math.random() * 20));
    };

    return (
        <div>
            UseContext{' '}
            <Button
                type="primary"
                style={{ marginLeft: 10 }}
                onClick={() => setContext()}
            >
                context
            </Button>
            <myContext.Provider value={{ time, randon }}>
                <GrandFather></GrandFather>
            </myContext.Provider>
        </div>
    );
};

export default UseContext;
