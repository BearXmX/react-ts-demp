/*
 * @Date: 2021-09-19 13:39:50
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-19 20:57:36
 * @Description: useRef
 */
import { useRef, useState } from 'react';
import { Button } from 'antd';
import Son from '../../../components/use-ref/son';

const UseRef: React.FC = () => {
    const [str, setStr] = useState(0);
    const MyRef: any = useRef();

    const getInfo = () => {
        const res = MyRef.current.getCount();
        setStr(res);
    };

    return (
        <div>
            <Button
                type="default"
                onClick={() => getInfo()}
                style={{ marginRight: 20 }}
            >
                UseRef
            </Button>
            <span>get son state : {str} </span>
            <Son ref={MyRef}></Son>
        </div>
    );
};

export default UseRef;
