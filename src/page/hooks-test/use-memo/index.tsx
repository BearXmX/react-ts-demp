/*
 * @Date: 2021-09-19 21:06:43
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-19 21:47:55
 * @Description: useMemo
 */
import { useState } from 'react';
import { Button } from 'antd';

import Son from '../../../components/use-memo/son';
const UseMemo: React.FC = () => {
    const [count, setCount] = useState(0);

    return (
        <div>
            UseMemo {count}
            <Button
                style={{ marginLeft: 10 }}
                onClick={() => setCount(count + 1)}
            >
                father change
            </Button>
            <Son></Son>
        </div>
    );
};

export default UseMemo;
