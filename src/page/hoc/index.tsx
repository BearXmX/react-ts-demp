/*
 * @Date: 2021-09-25 13:59:51
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-25 16:01:11
 * @Description: hoc
 */
import HocSon from '../../components/hoc/son';

const Hoc: React.FC = () => {
    return (
        <div>
            Hoc
            <HocSon></HocSon>
        </div>
    );
};

export default Hoc;
