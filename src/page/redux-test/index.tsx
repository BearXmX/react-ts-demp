/*
 * @Date: 2021-09-12 17:51:24
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-12 22:31:30
 * @Description: redux测试页面
 */
import { connect } from 'react-redux';
import { Button } from 'antd';

const ReduxTest: React.FC = (props: any) => {
    console.log('更新了');
    /**
     * @description: redux ++
     */
    const addRedux = () => {
        let count = props.count;
        count++;
        props.addAction(count);
    };

    /**
     * @description: redux --
     */
    const reduceRedux = () => {
        let count = props.count;
        count--;
        props.reduceAction(count);
    };

    return (
        <>
            <div>
                <Button
                    type="primary"
                    style={{ marginRight: 10 }}
                    onClick={() => addRedux()}
                >
                    redux + 1
                </Button>
                <Button onClick={() => reduceRedux()}>redux - 1</Button>
            </div>
            <div style={{ marginTop: 10, fontSize: 16 }}>
                <h1>{props.count}</h1>
            </div>
        </>
    );
};

const mapStateToProps = (state: any) => {
    return state;
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        addAction: (count: number) => {
            dispatch({
                type: 'add',
                payload: count,
            });
        },
        reduceAction: (count: number) => {
            dispatch({
                type: 'reduce',
                payload: count,
            });
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(ReduxTest);
