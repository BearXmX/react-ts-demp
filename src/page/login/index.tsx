/*
 * @Date: 2021-09-05 13:17:40
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-05 19:25:37
 * @Description: 登录页面
 */
import { Form, Input, Button } from 'antd';
import LoginImg from '../../assets/image/login.jpg';
import { withRouter} from 'react-router-dom';
import './index.less';

const Login: React.FC = (props: any) => {
    const onFinish = (values: any) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);              
    };
    return (
        <div className="login">
            <div className="login-main">
                <div className="login-main-left">
                    <img src={LoginImg} alt="" />
                </div>
                <div className="login-main-right">
                    <Form
                        name="basic"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <h1>欢迎登录</h1>
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Button type="primary" htmlType="submit">
                                登录
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
};

export default withRouter(Login);
