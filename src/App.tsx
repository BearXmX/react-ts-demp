/*
 * @Date: 2021-08-08 12:31:26
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-09-25 14:01:36
 * @Description:
 */
import { useEffect, createContext } from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import Home from '../src/page/home';
import Login from './page/login';
import Cockpit from './page/cockpit';
import ReduxTest from './page/redux-test';
import UseContext from './page/hooks-test/use-context';
import UseRef from './page/hooks-test/use-ref';
import UseMemo from './page/hooks-test/use-memo';
import Hoc from './page/hoc';
import './assets/css/App.css';

export const SonContext = createContext({});
interface routes {
    path: string;
    component: React.ReactNode;
}
const App: React.FC = () => {
    const ROUTE_PREFIX: string = '/home';
    const HOME_ROUTES: routes[] = [
        {
            path: ROUTE_PREFIX + '/cockpit',
            component: Cockpit,
        },
        {
            path: ROUTE_PREFIX + '/redux',
            component: ReduxTest,
        },
        {
            path: ROUTE_PREFIX + '/useContext',
            component: UseContext,
        },
        {
            path: ROUTE_PREFIX + '/useRef',
            component: UseRef,
        },
        {
            path: ROUTE_PREFIX + '/useMemo',
            component: UseMemo,
        },
        {
            path: ROUTE_PREFIX + '/hoc',
            component: Hoc,
        },
    ];
    useEffect(() => {}, []);
    return (
        <HashRouter>
            <Provider store={store}>
                <div className="App">
                    <Switch>
                        <Route exact path="/login" component={Login} />
                        <Route
                            path="/home"
                            render={() => (
                                <Home>
                                    {HOME_ROUTES.map((item) => {
                                        return (
                                            <Route
                                                key={item.path}
                                                exact
                                                path={item.path}
                                                component={item.component}
                                            />
                                        );
                                    })}
                                </Home>
                            )}
                        />
                        <Redirect exact from="/" to="/home/cockpit"></Redirect>
                    </Switch>
                </div>
            </Provider>
        </HashRouter>
    );
};

export default App;
